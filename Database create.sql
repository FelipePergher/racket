-- ----------------------------------------------------------------------------
-- MySQL Workbench Migration
-- Migrated Schemata: racket
-- Source Schemata: racket
-- Created: Sun Oct 22 21:30:25 2017
-- Workbench Version: 6.3.9
-- ----------------------------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------------------------------------------------------
-- Schema racket
-- ----------------------------------------------------------------------------
DROP SCHEMA IF EXISTS `racket` ;
CREATE SCHEMA IF NOT EXISTS `racket` ;

-- ----------------------------------------------------------------------------
-- Table racket.db_questions
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `racket`.`db_questions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `question` VARCHAR(300) NULL DEFAULT NULL,
  `option1` VARCHAR(300) NULL DEFAULT NULL,
  `option2` VARCHAR(300) NULL DEFAULT NULL,
  `option3` VARCHAR(300) NULL DEFAULT NULL,
  `option4` VARCHAR(300) NULL DEFAULT NULL,
  `option5` VARCHAR(300) NULL DEFAULT NULL,
  `answer` VARCHAR(300) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 26
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table racket.questions
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `racket`.`questions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `db_questions_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_questions_db_questions_idx` (`db_questions_id` ASC),
  CONSTRAINT `fk_questions_db_questions`
    FOREIGN KEY (`db_questions_id`)
    REFERENCES `racket`.`db_questions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8;
SET FOREIGN_KEY_CHECKS = 1;
